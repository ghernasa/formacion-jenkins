package common;

import boundary.CarResource;
import exception.MasterExceptionMapper;

import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Configures a JAX-RS endpoint. Delete this class, if you are not exposing
 * JAX-RS resources in your application.
 *
 * @author airhacks.com
 */
@ApplicationPath("/services")
public class JAXRSConfiguration extends Application {

    public Set<Class<?>> getClasses() {
        Set<Class<?>> s = new HashSet<Class<?>>();
        s.add(CarResource.class);

        /**
         * you need to add ExceptionMapper class as well *
         */
        s.add(MasterExceptionMapper.class);
        return s;
    }
}
