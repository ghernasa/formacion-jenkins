
package exception;

import javax.validation.ConstraintViolationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 *
 * @author jlopemes
 */
@Provider
public class ConstraintExceptionMapper implements ExceptionMapper<ConstraintViolationException>{
    @Override
    public Response toResponse(final ConstraintViolationException exception){
        return Response.status(Response.Status.BAD_REQUEST)
                .type(MediaType.APPLICATION_JSON_TYPE)
                .entity(exception.getMessage())
                .build();
    }
    
}
