package exception;

import javax.ejb.EJBException;
import javax.ejb.EJBTransactionRolledbackException;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


@Provider
public class MasterExceptionMapper implements ExceptionMapper<EJBTransactionRolledbackException> {


    @Override
    public Response toResponse(final EJBTransactionRolledbackException exception) {
        Exception e = getRootCause(exception);
        List<String> eMessages=new ArrayList<>();
        Response.Status status=Response.Status.INTERNAL_SERVER_ERROR;
        if (e instanceof ConstraintViolationException){
            Set<ConstraintViolation<?>> constraintViolations = ((ConstraintViolationException) e).getConstraintViolations();
            eMessages = constraintViolations.stream().map(
                    ConstraintViolation::getMessage).collect(Collectors.toList());
            status = Response.Status.BAD_REQUEST;
        }
        if(e instanceof NullPointerException){
            eMessages.add(e.getMessage());
            status = Response.Status.EXPECTATION_FAILED;
        }
        if(e instanceof IllegalStateException){
            eMessages.add(e.getMessage());
            status = Response.Status.EXPECTATION_FAILED;
        }
        if(e instanceof NotFoundException){
            eMessages.add(e.getMessage());
            status=Response.Status.NOT_FOUND;
        }
        if(e instanceof IllegalArgumentException){
            eMessages.add(e.getMessage());
            status=Response.Status.BAD_REQUEST;
        } else{
            eMessages.add(e.getMessage());
        }
        return Response.status(status)
                .entity(eMessages)
                .type(MediaType.APPLICATION_JSON)
                .build();


    }
    public static Exception getRootCause ( EJBException exception )
    {
        if ( null == exception ) {
            return null;
        }

        EJBException effect = exception;
        Exception cause = effect.getCausedByException();

        while (cause instanceof EJBException) {
            effect = (EJBException) cause;
            cause = effect.getCausedByException();
        }

        return null == cause ? effect : cause;
    }
}