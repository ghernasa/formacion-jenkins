package entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.sql.Timestamp;
import java.util.Objects;

/**
 *
 * @author jlopemes
 */
@Entity
@NamedQueries({
        @NamedQuery(name="Contador", query="SELECT c FROM Car c WHERE c.brand=:brand AND c.country=:country")
})
@Table(name = "Car")
public class Car {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "brand")
    @NotNull(message = "Please insert a brand")
    @Size(min = 3, max = 20, message = "Brand name must be between 3 and 20 characters")
    private String brand;

    @Column(name="country")
    @NotNull(message = "Please insert a country")
    private String country;

    @Column(name="created_at",
            updatable = false,
            columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Timestamp created;

    @Column(name="last_updated",
            columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Timestamp updated;

    @Column(name="registration")
    @NotNull(message = "Please insert a registration date")
    private Timestamp registration;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    public Timestamp getUpdated() {
        return updated;
    }

    public void setUpdated(Timestamp updated) {
        this.updated = updated;
    }

    public Timestamp getRegistration() {
        return registration;
    }

    public void setRegistration(Timestamp registration) {
        this.registration = registration;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return brand.equals(car.brand) && country.equals(car.country) && registration.equals(car.registration);
    }

    @Override
    public int hashCode() {
        return Objects.hash(brand, country, registration);
    }
}
