
package boundary;

import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

/**
 *
 * @author jlopemes
 */
@Path("/Greetings")
public class HelloWorldWS {
    @GET
    @Path("HelloWorld")
    public Response sayHello(){
        Response.ResponseBuilder responseBuilder = Response.status(Response.Status.SERVICE_UNAVAILABLE);
        
        return responseBuilder.entity("Come back later").build();
    }
}
