
package boundary;


import control.CarService;
import dto.DTOSelectBulk;
import entity.Car;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 *
 * @author jlopemes
 */
@Path("cars")
public class CarResource {

    @Inject
    CarService carService;
    /**
     * This method is used to get a car list
     * @return Response
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCars(){
        return Response.ok().entity(carService.getCars()).build();
    }

    /**
     * This method is used to get a car by id
     * @param id id that identifies the desired car on database
     * @return Response
     */
    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCarById(@PathParam("id") long id){
        return Response.ok().entity(carService.getCarById(id)).build();
    }

    /**
     * This method is used to create a car
     * @param car said car that wants to be created on database
     * @return Response
     */
    @Transactional
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createCar(Car car){
        return Response.ok().entity(carService.createCar(car)).build();
    }

    /**
     * This method is used to create multiple cars
     * @param carList said car list that wants to be created on database
     * @return Response
     */
    @Transactional
    @POST
    @Path("create-in-bulk")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createMultipleCars(List<Car> carList){
        return Response.ok().entity(carService.createMultipleCars(carList)).build();
    }
    /**
     * This method is used to update a car
     * @param car car with all new desired values apart from id
     * @return Response
     */
    @Transactional
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateCar(Car car){
        return Response.ok().entity(carService.updateCar(car)).build();
    }
    /**
     * This method is used to delete a car by id
     * @param id Car id that is wanted to be deleted
     * @return Response
     */
    @Transactional
    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteCar(long id){
        return Response.ok().entity(carService.deleteCarById(id)).build();
    }
    @Transactional
    @POST
    @Path("clone/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response CarClone(long id,Car car){
        return Response.ok().entity(carService.cloneCar(id,car)).build();
    }

    /**
     * Obtains either id(s), id(s) and date(s) or date(s) to delete in bulk;
     * if id(s) are passed, cars found with said id(s) will be removed.
     * If dates are passed, cars created between both dates will be removed.
     * If one date is passed, cars created after said date will be removed.
     * If id(s) and date(s) are combined, both date(s) and id(s) cases are applied together.
     * @param data data received from request having id(s) and/or date(s)
     * @return Response
     */
    @Transactional
    @POST
    @Path("delete-in-bulk")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteCarsInBulk(DTOSelectBulk data){
        return Response.ok().entity(carService.deleteCarsInBulk(data)).build();
    }
    /**
     * This method is used to get a car list ordered by criteria specified on query params
     * These query params will be sort and order
     * @param sort is the criteria which will order the list
     * the values should be created or updated
     * by default the value will be created
     * @param order is the order of the criteria of the returning list
     * the values should be desc or asc
     * by default the value will be desc
     * @return List of cars ordered
     */
    @GET
    @Path("/sort")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCarsOrdered(@DefaultValue("created")@QueryParam("sort") String sort,
                                   @DefaultValue("desc")@QueryParam("order") String order){
        return Response.ok().entity(carService.getCarsOrdered(sort, order)).build();
    }
}
