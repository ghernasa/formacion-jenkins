
package control;

import dto.DTOSelectBulk;
import entity.Car;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.ws.rs.NotFoundException;
import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author jlopemes
 */
@ApplicationScoped
public class CarService {
    @PersistenceContext(name = "pu-formacion")
    EntityManager entityManager;

    /**
     * This method is used to get a car list
     * @return List<Car>
     */
    public List<Car> getCars(){
        Query query = entityManager.createQuery("SELECT c FROM Car c");
        return query.getResultList();
    }

    /**
     * This method is used to get a car by id
     * @param id Car id
     * @return Car
     */
    public Car getCarById(long id){
        Car car=entityManager.find(Car.class, id);
        if(car==null){
            throw new NotFoundException("Given car was not found on database.");
        }
        return car;
    }

    /**
     * This method is used to create a car
     * @param car Object car
     * @return Car
     */
    public Car createCar(Car car){
        if(car!=null) {
            Timestamp timestamp = new Timestamp(new Date().getTime());
            car.setUpdated(timestamp);
            car.setCreated(timestamp);
            entityManager.persist(car);
        }
        else{
            throw new NullPointerException("No new car given.");
        }
        return car;
    }

    /**
     * This method is used to create a car list in bulk
     * @param carList List of car
     * @return List of car
     */
    public List<Car> createMultipleCars(List<Car> carList){
        if(carList == null ) {
            throw new NullPointerException("List is null.");
        }else if (carList.isEmpty()){
            throw new IllegalStateException("List is empty.");
        }else {
            Timestamp timestamp = new Timestamp(new Date().getTime());
            List<Car> carsDataBase = getCars();
            List<Car> noDuplicatedList = removeDuplicatedCars(carList);
            carList = checkListInDatabase(noDuplicatedList,carsDataBase);

            for (Car car: carList) {
                car.setUpdated(timestamp);
                car.setCreated(timestamp);
                entityManager.persist(car);
            }
        }
        return carList;
    }

    /**
     * This method is used to update a car
     * @param car Object car
     * @return Car
     */
    public Car updateCar(Car car) {
        Car result = entityManager.find(Car.class, car.getId());
        if(result != null) {
                result.setBrand(car.getBrand());
                result.setCountry(car.getCountry());
                result.setUpdated(new Timestamp(new Date().getTime()));
                result.setRegistration(car.getRegistration());
        }
        else{
            throw new NotFoundException("Given car was not found on database.");
        }
        return result;
    }

    /**
     * This method is used to delete a car by id
     * @param id Car id
     * @return Car
     */
    public Car deleteCarById(long id){
        Car car = entityManager.find(Car.class, id);
        if(car!=null){
            entityManager.remove(car);
        }
        else{
            throw new NotFoundException("Given car was not found on database.");
        }
        return car;
    }

    /**
     * Obtains either id(s), id(s) and date(s) or date(s) to delete in bulk;
     * if id(s) are passed, cars found with said id(s) will be removed.
     * If dates are passed, cars created between both dates will be removed.
     * If one date is passed, cars created after said date will be removed.
     * If id(s) and date(s) are combined, both date(s) and id(s) cases are applied together.
     * @param data data recieved from request having id(s) and/or date(s)
     * @return deleted cars
     */
    public List<Car> deleteCarsInBulk(DTOSelectBulk data){
        //if there is an id list, it gets retrieved
        List<Long> ids = new ArrayList<>();
        if(data.getIds() != null){
            ids = data.getIds();
        }

        //if there is a start date, it gets retrieved
        Timestamp startDate = null;
        if(data.getStartDate() != null){
            startDate = data.getStartDate();
        }
        //if there is an end date, it gets retrieved
        Timestamp endDate = null;
        if(data.getEndDate() != null){
            endDate = data.getEndDate();
        }

        if(startDate.equals(null) && ids.isEmpty()){
            throw new NullPointerException("No data has been given.");
        }
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Car> c = cb.createQuery(Car.class);
        Root<Car> root = c.from(Car.class);

        //if there is no ids, ids clause on query is skipped
        Predicate idsPredicate = null;
        if(!ids.isEmpty()){
            idsPredicate = cb.and(root.get("id").in(ids));
        }

        Predicate datePredicate=null;
        //when there are dates...
        if(startDate!=null){
            if(endDate != null){
                    datePredicate = cb.between(root.get("created"),
                            startDate.toString(),
                            endDate.toString());
            }else {
                    datePredicate = cb.greaterThan(root.get("created"),
                            startDate.toString());
            }
        }
        if(!ids.isEmpty() && startDate!=null){
            c.where(cb.and(idsPredicate,datePredicate));
        }else if(!ids.isEmpty()){
            c.where(idsPredicate);
        }else{
            c.where(datePredicate);
        }
        TypedQuery<Car> query = entityManager.createQuery(c);
        List<Car> cars = query.getResultList();
        if(!cars.isEmpty()) {
            for (Car car : cars) {
                entityManager.remove(car);
            }
            ;
        }else{
            throw new NullPointerException("There is no car with given parameters.");
        }
        return cars;
    }

    /**
     * This method is used to clone a preexisting car and modify its values (Country and Brand),
     * the method is fed a Json containing both these variables and registration because it can't
     * be null.
     * First it is checked that the cloned car is not null if it is it throws an exception, then since
     * we only are interesed in brand and country we set the other variables from scratch.
     * Then we check if the variables in the Json aren't null and if they are the cloned original values
     * are picked.
     * There are several exceptions on this method such as the cloned car can't have the same values (Country
     * and Brand) so there won't be any duplicates, there is also a query that searches the car in the database
     * before inserting it to check if it already exists.
     * @param id id of the vehicle about to be cloned
     * @param car Object car about to be cloned
     * @return car
     */

    public Car cloneCar(long id,Car car) {
        Car clonedData = entityManager.find(Car.class, id);
        Car cloned = new Car();
        if(car!=null) {
            Timestamp timestamp = new Timestamp(new Date().getTime());
            cloned.setUpdated(timestamp);
            cloned.setCreated(timestamp);
            if(car.getBrand()==null){
                cloned.setBrand(clonedData.getBrand());
            } else {
                cloned.setBrand(car.getBrand());
            }
            if(car.getCountry()==null){
                cloned.setCountry(clonedData.getCountry());
            } else {
                cloned.setCountry(car.getCountry());
            }
            if(car.getRegistration()==null){
                cloned.setRegistration(clonedData.getRegistration());
            } else {
                cloned.setRegistration(car.getRegistration());
            }
            TypedQuery<Car> namedQuery= entityManager.createNamedQuery("Contador",Car.class);
            namedQuery.setParameter("brand",cloned.getBrand());
            namedQuery.setParameter("country",cloned.getCountry());
            List<Car> query= namedQuery.getResultList();
            Set<Car> carSet= new HashSet<>();
            for (Car vehicle: query) {
                carSet.add(vehicle);
            }
            int size= carSet.size();
            carSet.add(cloned);
            if(size==carSet.size()) {
                throw new IllegalArgumentException("This object is already in the database");
            } else {
                entityManager.persist(cloned);
            }
            return cloned;
        }
        else{
            throw new NullPointerException("The cloned car was null");
        }

    }


    /**
     * This helper method remove duplicates from a given list<Car>
     * @param carList Given list
     * @return List of cars
     */
    private List<Car> removeDuplicatedCars(List<Car> carList){
        return carList
                .stream()
                .distinct()
                .collect(Collectors.toList());
    }

    /**
     * This helper method compares given car list with database car list, then return a list without duplicates.
     * @param carList List of cars
     * @param carsDataBase List of cars in database
     * @return List of cars
     */
    private List<Car> checkListInDatabase(List<Car> carList,List<Car> carsDataBase){
        List<Car> checkedList = new ArrayList<>();
        Boolean isValid;
        for (Car car : carList) {
            isValid = true;
            if (car.getBrand().isEmpty() || car.getBrand() == null ||
                    car.getCountry().isEmpty() || car.getCountry() == null ||
                    car.getRegistration() == null){
                isValid = false;
            }else{
                for (Car carDB : carsDataBase){
                    if (car.equals(carDB)) {
                        isValid = false;
                        break;
                    }
                }
            }

            if (isValid){
                checkedList.add(car);
            }
        }
        return checkedList;
    }

    /**
     * This method is used to get all Cars objects from the DB ordered by criteria from incoming query params sort and order
     * @return List with results ordered
     */
    public List getCarsOrdered(String sort, String order){
        if ((sort.equals("created") || sort.equals("updated")) && (order.equals("desc") || order.equals("asc")))
        {
        Query query = entityManager.createQuery("SELECT c FROM Car c ORDER BY c."+sort+" "+order);

        return query.getResultList();}
    else throw  new IllegalArgumentException("please insert valid sort and order query params");
    }
}
