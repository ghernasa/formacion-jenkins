/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package boundary;

import control.CarService;
import dto.DTOSelectBulk;
import entity.Car;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.ws.rs.core.Response;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

/**
 *
 * @author jlopemes
 */
@RunWith(MockitoJUnitRunner.class)
public class CarResourceTest {
    @Mock
    CarService carService;
    @InjectMocks
    CarResource carResource;

    /**
     * Test of getCars method, of class CarResource.
     */
    @Test
    public void testGetCars() {
        Car car = new Car();
        car.setId(1);
        car.setBrand("BMW");
        car.setCountry("ES");
        car.setCreated(new Timestamp(92835735));
        car.setUpdated(new Timestamp(92835735));
        car.setRegistration(new Timestamp(92835735));
        
        List<Car> cars = new ArrayList<>();
        cars.add(car);

        when(carService.getCars()).thenReturn(cars);
        Response res = carResource.getCars();
        Response test = Response.ok().entity(cars).build();

        assertNotNull(cars);
        assertEquals(res.getEntity(),test.getEntity());
        assertEquals(res.getStatusInfo().getFamily(),Response.Status.Family.SUCCESSFUL);
    }

    /**
     * Test of getCarById method, of class CarResource.
     */
    @Test
    public void testGetCarById() {
        Car car = new Car();
        car.setId(1);
        car.setBrand("BMW");
        car.setCountry("ES");
        car.setCreated(new Timestamp(92835735));
        car.setUpdated(new Timestamp(92835735));
        car.setRegistration(new Timestamp(92835735));

        when(carService.getCarById(1)).thenReturn(car);
        Response res = carResource.getCarById(1);
        Response test = Response.ok().entity(car).build();

        assertEquals(res.getEntity(),test.getEntity());
        assertEquals(res.getStatusInfo().getFamily(),Response.Status.Family.SUCCESSFUL);
    }

    /**
     * Test of createCar method, of class CarResource.
     */
    @Test
    public void testCreateCar() {
        Car car = new Car();
        car.setId(1);
        car.setBrand("BMW");
        car.setCountry("ES");
        car.setCreated(new Timestamp(92835735));
        car.setUpdated(new Timestamp(92835735));
        car.setRegistration(new Timestamp(92835735));

        when(carService.createCar(car)).thenReturn(car);
        Response res = carResource.createCar(car);
        Response test = Response.ok().entity(car).build();

        assertEquals(res.getEntity(),test.getEntity());
        assertEquals(res.getStatusInfo().getFamily(),Response.Status.Family.SUCCESSFUL);
    }

    /**
     * Test of createMultipleCars method, of class CarResource.
     */
    @Test
    public void testCreateMultipleCars(){
        Car car = new Car();
        car.setId(1);
        car.setBrand("BMW");
        car.setCountry("ES");
        car.setCreated(new Timestamp(92835735));
        car.setUpdated(new Timestamp(92835735));
        car.setRegistration(new Timestamp(92835735));

        List<Car> testList = new ArrayList<>();
        testList.add(car);

        when(carService.createMultipleCars(testList)).thenReturn(testList);
        Response res = carResource.createMultipleCars(testList);

        assertEquals(res.getEntity(),testList);
        assertEquals(res.getStatusInfo().getFamily(),Response.Status.Family.SUCCESSFUL);
    }

    /**
     * Test of updateCar method, of class CarResource.
     */
    @Test
    public void testUpdateCar() {
        Car car = new Car();
        car.setId(1);
        car.setBrand("BMW");
        car.setCountry("ES");
        car.setCreated(new Timestamp(92835735));
        car.setUpdated(new Timestamp(92835735));
        car.setRegistration(new Timestamp(92835735));
        Car updatedCar = new Car();
        updatedCar.setId(1);
        updatedCar.setBrand("Nissan");
        updatedCar.setCountry("ES");
        updatedCar.setCreated(new Timestamp(92835735));
        updatedCar.setUpdated(new Timestamp(92835735));
        car.setRegistration(new Timestamp(92835735));

        when(carService.updateCar(car)).thenReturn(updatedCar);
        Response res = carResource.updateCar(car);
        Response test = Response.ok().entity(updatedCar).build();

        assertEquals(res.getEntity(),test.getEntity());
        assertEquals(res.getStatusInfo().getFamily(),Response.Status.Family.SUCCESSFUL);
    }

    /**
     * Test of deleteCar method, of class CarResource.
     */
    @Test
    public void testDeleteCar() {
        Car car = new Car();
        car.setId(1);
        car.setBrand("BMW");
        car.setCountry("ES");
        car.setCreated(new Timestamp(92835735));
        car.setUpdated(new Timestamp(92835735));
        car.setRegistration(new Timestamp(92835735));

        when(carService.deleteCarById(1)).thenReturn(car);
        Response res = carResource.deleteCar(1);
        Response test = Response.ok().entity(car).build();

        assertEquals(res.getEntity(),test.getEntity());
        assertEquals(res.getStatusInfo().getFamily(),Response.Status.Family.SUCCESSFUL);
    }

    @Test
    public void testDeleteCarInBulk() {
        Car car = new Car();
        car.setId(1);
        car.setBrand("BMW");
        car.setCountry("ES");
        car.setCreated(new Timestamp(92835735));
        car.setUpdated(new Timestamp(92835735));
        car.setRegistration(new Timestamp(92835735));
        List<Car> carList=new ArrayList<>();
        carList.add(car);
        DTOSelectBulk data=new DTOSelectBulk();
        when(carService.deleteCarsInBulk(data)).thenReturn(carList);
        Response res = carResource.deleteCarsInBulk(data);
        Response test = Response.ok().entity(carList).build();

        assertEquals(res.getEntity(),test.getEntity());
        assertEquals(res.getStatusInfo().getFamily(),Response.Status.Family.SUCCESSFUL);
    }
    /**
     * Test of deleteCar method, of class CarResource.
     */
    @Test
    public void testgetCarsOrdered() {
        Car car = new Car();
        car.setId(1);
        car.setBrand("BMW");
        car.setCountry("GER");
        car.setCreated(new Timestamp(95835735));
        car.setUpdated(new Timestamp(92835735));
        car.setRegistration(new Timestamp(92835735));
        List<Car> carsMock = new ArrayList<>();
        carsMock.add(car);
        car.setId(2);
        car.setCreated(new Timestamp(92835735));
        car.setUpdated(new Timestamp(95835735));
        carsMock.add(car);
        String sort = "created";
        String order = "desc";
        when(carService.getCarsOrdered(sort, order)).thenReturn(carsMock);
        Response res = carResource.getCarsOrdered(sort, order);
        Response test = Response.ok().entity(carsMock).build();

        assertEquals(res.getEntity(), test.getEntity());
        assertEquals(res.getStatusInfo().getFamily(), Response.Status.Family.SUCCESSFUL);
    }
}
