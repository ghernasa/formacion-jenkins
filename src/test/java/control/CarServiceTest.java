/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import dto.DTOSelectBulk;
import entity.Car;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Answers.RETURNS_SELF;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 *
 * @author jlopemes
 */

@RunWith(MockitoJUnitRunner.class)
public class CarServiceTest {
    @Mock
    EntityManager entityManagerMock;
    @InjectMocks
    CarService carService;
    Car testCar;

    /**
     * Test of getCars method, of class CarService.
     */
    @Test
    public void testGetCars() {
        testCar = new Car();
        testCar.setId(1);
        testCar.setBrand("BMW");
        testCar.setCountry("ES");
        testCar.setCreated(new Timestamp(92835735));
        testCar.setUpdated(new Timestamp(92835735));
        testCar.setRegistration(new Timestamp(92835735));

        Query query = mock(Query.class);
        when(entityManagerMock.createQuery("SELECT c FROM Car c")).thenReturn(query);

        List<Car> carListResult = carService.getCars();
        assertNotNull(carListResult);
    }

    /**
     * Test of getCarById method, of class CarService.
     */
    @Test
    public void testGetCarById() {
        testCar = new Car();
        testCar.setId(1);
        testCar.setBrand("BMW");
        testCar.setCountry("ES");
        testCar.setCreated(new Timestamp(92835735));
        testCar.setUpdated(new Timestamp(92835735));
        testCar.setRegistration(new Timestamp(92835735));

        when(entityManagerMock.find(Car.class,testCar.getId())).thenReturn(testCar);
        Car result = carService.getCarById(1);

        assertEquals(result.getId(),testCar.getId());
        assertEquals(result.getBrand(),testCar.getBrand());
    }

    /**
     * Test of createCar method, of class CarService.
     */
    @Test
    public void testCreateCar() {
        testCar = new Car();
        testCar.setId(1);
        testCar.setBrand("BMW");
        testCar.setCountry("ES");
        testCar.setCreated(new Timestamp(92835735));
        testCar.setUpdated(new Timestamp(92835735));
        testCar.setRegistration(new Timestamp(92835735));

        entityManagerMock.persist(testCar);
        Car result = carService.createCar(testCar);
        assertEquals(result.getId(),testCar.getId());
        assertEquals(result.getBrand(),testCar.getBrand());
    }

    /**
     * Test of createMultipleCars method, of class CarService.
     */
    @Test
    public void testCreateMultipleCars(){
        testCar = new Car();
        testCar.setId(1);
        testCar.setBrand("BMW");
        testCar.setCountry("ES");
        testCar.setCreated(new Timestamp(92835735));
        testCar.setUpdated(new Timestamp(92835735));
        testCar.setRegistration(new Timestamp(92835735));

        List<Car> testList = new ArrayList<>();
        testList.add(testCar);

        Query query = mock(Query.class);
        when(entityManagerMock.createQuery("SELECT c FROM Car c")).thenReturn(query);

        List<Car> carListResult = carService.createMultipleCars(testList);
        assertNotNull(carListResult);
        assertEquals(carListResult,testList);
    }

    /**
     * Test of updateCar method, of class CarService.
     */
    @Test
    public void testUpdateCar() {
        testCar = new Car();
        testCar.setId(1);
        testCar.setBrand("BMW");
        testCar.setCountry("ES");
        testCar.setCreated(new Timestamp(92835735));
        testCar.setUpdated(new Timestamp(92835735));
        testCar.setRegistration(new Timestamp(92835735));
        Car updatedCar = new Car();
        updatedCar.setId(1);
        updatedCar.setBrand("Opel");
        testCar.setCountry("ES");
        testCar.setCreated(new Timestamp(92835735));
        testCar.setUpdated(new Timestamp(92835735));
        testCar.setRegistration(new Timestamp(92835735));

        when(entityManagerMock.find(Car.class,testCar.getId())).thenReturn(updatedCar);
        Car result = carService.updateCar(testCar);
        assertEquals(result.getId(),testCar.getId());
        assertEquals(result.getBrand(),testCar.getBrand());
    }

    /**
     * Test of deleteCarById method, of class CarService.
     */
    @Test
    public void testDeleteCarById() {
        testCar = new Car();
        testCar.setId(1);
        testCar.setBrand("BMW");
        testCar.setCountry("ES");
        testCar.setCreated(new Timestamp(92835735));
        testCar.setUpdated(new Timestamp(92835735));
        testCar.setRegistration(new Timestamp(92835735));

        when(entityManagerMock.find(Car.class,testCar.getId())).thenReturn(testCar);
        Car result = carService.deleteCarById(1);
        assertEquals(result.getId(),testCar.getId());
        assertEquals(result.getBrand(),testCar.getBrand());
    }

    /**
     *Test 1º of carCloned method, of class CarService
     * In this test we check the method by asserting that the cloned car doesn't have
     * the same id, brand and country being a different car altogether
     */

    @Test
    public void testCarCloned1() {
        testCar = new Car();
        testCar.setId(1);
        testCar.setBrand("BMW");
        testCar.setCountry("Francia");
        testCar.setCreated(new Timestamp(92835735));
        testCar.setUpdated(new Timestamp(92835735));
        testCar.setRegistration(new Timestamp(92835735));
        Car cloned = new Car();
        cloned.setBrand("Opel");
        cloned.setCountry("China");
        cloned.setRegistration(new Timestamp(92835735));
        TypedQuery typedQuery=mock(TypedQuery.class);
        when(entityManagerMock.createNamedQuery("Contador",Car.class)).thenReturn(typedQuery);
        entityManagerMock.persist(testCar);
        when(entityManagerMock.find(Car.class,testCar.getId())).thenReturn(testCar);
        Car result = carService.cloneCar(testCar.getId(), cloned);
        assertNotEquals(result,testCar);
        assertNotEquals(result.getId(),testCar.getId());
        assertNotEquals(result.getCountry(),testCar.getCountry());
        assertNotEquals(result.getBrand(),testCar.getBrand());
    }

    @Test
    public void testDeleteCarsInBulkIdsAndDates(){
        List<Long> ids=new ArrayList<>();
        ids.add((long)1);
        ids.add((long)2);
        ids.add((long)3);
        DTOSelectBulk data=new DTOSelectBulk();
        data.setIds(ids);
        data.setStartDate(new Timestamp(1652078896));
        data.setEndDate(new Timestamp(1652078896));
        testCar = new Car();
        testCar.setId(1);
        testCar.setBrand("BMW");
        testCar.setCountry("ES");
        testCar.setCreated(new Timestamp(92835735));
        testCar.setUpdated(new Timestamp(92835735));
        testCar.setRegistration(new Timestamp(92835735));
        List<Car> carsMock=new ArrayList<>();
        carsMock.add(testCar);
        TypedQuery<Car> query=mock(TypedQuery.class,RETURNS_SELF);
        CriteriaBuilder cb =mock(CriteriaBuilder.class,RETURNS_SELF);
        CriteriaQuery<Car> c = mock(CriteriaQuery.class,RETURNS_SELF);
        Root<Car> root = mock(Root.class,RETURNS_SELF);
        when(entityManagerMock.getCriteriaBuilder()).thenReturn(cb);
        when(cb.createQuery(Car.class)).thenReturn(c);
        when(c.from(Car.class)).thenReturn(root);
        when(entityManagerMock.createQuery(c)).thenReturn(query);
        when(query.getResultList()).thenReturn(carsMock);
        List<Car> result=carService.deleteCarsInBulk(data);
        assertEquals(carsMock,result);
    }

    @Test
    public void testDeleteCarsInBulkNoData(){
        DTOSelectBulk data=new DTOSelectBulk();
        assertThrows("No data has been given.",NullPointerException.class,()->carService.deleteCarsInBulk(data));
    }

    @Test
    public void testDeleteCarsInBulkNoFoundCars(){
        List<Long> ids=new ArrayList<>();
        ids.add((long)1);
        ids.add((long)2);
        ids.add((long)3);
        DTOSelectBulk data=new DTOSelectBulk();
        data.setIds(ids);
        List<Car> carsMock=new ArrayList<>();
        assertThrows("There is no car with given parameters.",NullPointerException.class,()->carService.deleteCarsInBulk(data));
    }
    /**
     * Test of getCarsOrdered method, of class CarService with all possible json incomes.
     */
    //Case 1: no query params
    @Test
    public void testGetCarsOrdered_no_query_params() {
        testCar = new Car();
        testCar.setId(1);
        testCar.setBrand("BMW");
        testCar.setCountry("GER");
        testCar.setCreated(new Timestamp(95835735));
        testCar.setUpdated(new Timestamp(92835735));
        testCar.setRegistration(new Timestamp(92835735));
        List<Car> carsMock=new ArrayList<>();
        carsMock.add(testCar);
        testCar.setId(2);
        testCar.setCreated(new Timestamp(92835735));
        testCar.setUpdated(new Timestamp(95835735));
        carsMock.add(testCar);

        Query query = mock(Query.class);
        when(entityManagerMock.createQuery("SELECT c FROM Car c ORDER BY c.created desc")).thenReturn(query);
        // sort default value
        String sort = "created";
        // order specified on query params
        String order = "desc";
        when(query.getResultList()).thenReturn(carsMock);
        List carsOrderedService = carService.getCarsOrdered(sort, order);

        assertEquals(carsOrderedService,carsMock);
    }
    //Case 2: Order = created / sort missing from query params
    @Test
    public void testGetCarsOrdered_sort_created_no_order() {
        testCar = new Car();
        testCar.setId(1);
        testCar.setBrand("BMW");
        testCar.setCountry("GER");
        testCar.setCreated(new Timestamp(95835735));
        testCar.setUpdated(new Timestamp(92835735));
        testCar.setRegistration(new Timestamp(92835735));
        List<Car> carsMock=new ArrayList<>();
        carsMock.add(testCar);
        testCar.setId(2);
        testCar.setCreated(new Timestamp(92835735));
        testCar.setUpdated(new Timestamp(95835735));
        carsMock.add(testCar);

        Query query = mock(Query.class);
        when(entityManagerMock.createQuery("SELECT c FROM Car c ORDER BY c.created desc")).thenReturn(query);
        // sort specified on query params
        String sort = "created";
        // order default value
        String order = "desc";
        when(query.getResultList()).thenReturn(carsMock);
        List carsOrderedService = carService.getCarsOrdered(sort, order);

        assertEquals(carsOrderedService,carsMock);
    }
    //Case 3: Sort missing from query params / order  = desc
    @Test
    public void testGetCarsOrdered_no_sort_order_desc() {
        testCar = new Car();
        testCar.setId(1);
        testCar.setBrand("BMW");
        testCar.setCountry("GER");
        testCar.setCreated(new Timestamp(95835735));
        testCar.setUpdated(new Timestamp(92835735));
        testCar.setRegistration(new Timestamp(92835735));
        List<Car> carsMock=new ArrayList<>();
        carsMock.add(testCar);
        testCar.setId(2);
        testCar.setCreated(new Timestamp(92835735));
        testCar.setUpdated(new Timestamp(95835735));
        carsMock.add(testCar);

        Query query = mock(Query.class);
        when(entityManagerMock.createQuery("SELECT c FROM Car c ORDER BY c.created desc")).thenReturn(query);
        // sort default value
        String sort = "created";
        // order specified on query params
        String order = "desc";
        when(query.getResultList()).thenReturn(carsMock);
        List carsOrderedService = carService.getCarsOrdered(sort, order);

        assertEquals(carsOrderedService,carsMock);
    }
    //Case 4: Sort = created / order = desc
    @Test
    public void testGetCarsOrdered_order_created_sort_desc() {
        testCar = new Car();
        testCar.setId(1);
        testCar.setBrand("BMW");
        testCar.setCountry("GER");
        testCar.setCreated(new Timestamp(95835735));
        testCar.setUpdated(new Timestamp(92835735));
        testCar.setRegistration(new Timestamp(92835735));
        List<Car> carsMock=new ArrayList<>();
        carsMock.add(testCar);
        testCar.setId(2);
        testCar.setCreated(new Timestamp(92835735));
        testCar.setUpdated(new Timestamp(95835735));
        carsMock.add(testCar);

        Query query = mock(Query.class);
        when(entityManagerMock.createQuery("SELECT c FROM Car c ORDER BY c.created desc")).thenReturn(query);
        // sort specified on query params
        String sort = "created";
        // order specified on query params
        String order = "desc";
        when(query.getResultList()).thenReturn(carsMock);
        List carsOrderedService = carService.getCarsOrdered(sort, order);

        assertEquals(carsOrderedService,carsMock);
    }
    //Case 5: no Order  / sort = asc
    @Test
    public void testGetCarsOrdered_no_sort_order_asc() {
        testCar = new Car();
        testCar.setId(1);
        testCar.setBrand("BMW");
        testCar.setCountry("GER");
        testCar.setCreated(new Timestamp(91835735));
        testCar.setUpdated(new Timestamp(92835735));
        testCar.setRegistration(new Timestamp(92835735));
        List<Car> carsMock=new ArrayList<>();
        carsMock.add(testCar);
        testCar.setId(2);
        testCar.setCreated(new Timestamp(92835735));
        testCar.setUpdated(new Timestamp(95835735));
        carsMock.add(testCar);

        Query query = mock(Query.class);
        when(entityManagerMock.createQuery("SELECT c FROM Car c ORDER BY c.created asc")).thenReturn(query);
        // sort default value
        String sort = "created";
        // order specified on query params
        String order = "asc";
        when(query.getResultList()).thenReturn(carsMock);
        List carsOrderedService = carService.getCarsOrdered(sort, order);

        assertEquals(carsOrderedService,carsMock);
    }
    //Case 6: order = created  / sort = asc
    @Test
    public void testGetCarsOrdered_order_created_sort_asc() {
        testCar = new Car();
        testCar.setId(1);
        testCar.setBrand("BMW");
        testCar.setCountry("GER");
        testCar.setCreated(new Timestamp(95835735));
        testCar.setUpdated(new Timestamp(92835735));
        testCar.setRegistration(new Timestamp(92835735));
        List<Car> carsMock=new ArrayList<>();
        carsMock.add(testCar);
        testCar.setId(2);
        testCar.setCreated(new Timestamp(99835735));
        testCar.setUpdated(new Timestamp(95835735));
        carsMock.add(testCar);
        Query query = mock(Query.class);
        when(entityManagerMock.createQuery("SELECT c FROM Car c ORDER BY c.created asc")).thenReturn(query);
        // sort specified on query params
        String sort = "created";
        // order specified on query params
        String order = "asc";
        when(query.getResultList()).thenReturn(carsMock);
        List carsOrderedService = carService.getCarsOrdered(sort, order);

        assertEquals(carsOrderedService,carsMock);
    }
    //Case 7: sort = updated  / no order
    @Test
    public void testGetCarsOrdered_sort_updated_no_order() {
        testCar = new Car();
        testCar.setId(1);
        testCar.setBrand("BMW");
        testCar.setCountry("GER");
        testCar.setCreated(new Timestamp(92835735));
        testCar.setUpdated(new Timestamp(95835735));
        testCar.setRegistration(new Timestamp(92835735));
        List<Car> carsMock=new ArrayList<>();
        carsMock.add(testCar);
        testCar.setId(2);
        testCar.setCreated(new Timestamp(95835735));
        testCar.setUpdated(new Timestamp(92835735));
        carsMock.add(testCar);
        Query query = mock(Query.class);
        when(entityManagerMock.createQuery("SELECT c FROM Car c ORDER BY c.updated desc")).thenReturn(query);
        // sort specified on query params
        String sort = "updated";
        // order default value
        String order = "desc";
        when(query.getResultList()).thenReturn(carsMock);
        List carsOrderedService = carService.getCarsOrdered(sort, order);

        assertEquals(carsOrderedService,carsMock);
    }
    //Case 8: sort = updated  / order = desc
    @Test
    public void testGetCarsOrdered_order_updated_sort_desc() {
        testCar = new Car();
        testCar.setId(1);
        testCar.setBrand("BMW");
        testCar.setCountry("GER");
        testCar.setCreated(new Timestamp(92835735));
        testCar.setUpdated(new Timestamp(95835735));
        testCar.setRegistration(new Timestamp(92835735));
        List<Car> carsMock=new ArrayList<>();
        carsMock.add(testCar);
        testCar.setId(2);
        testCar.setCreated(new Timestamp(95835735));
        testCar.setUpdated(new Timestamp(92835735));
        carsMock.add(testCar);

        Query query = mock(Query.class);
        when(entityManagerMock.createQuery("SELECT c FROM Car c ORDER BY c.updated desc")).thenReturn(query);
        // sort specified on query params
        String sort = "updated";
        // order specified on query params
        String order = "desc";
        when(query.getResultList()).thenReturn(carsMock);
        List carsOrderedService = carService.getCarsOrdered(sort, order);

        assertEquals(carsOrderedService,carsMock);
    }
    //Case 9: order = updated  / sort = asc
    @Test
    public void testGetCarsOrdered_order_updated_sort_asc() {
        testCar = new Car();
        testCar.setId(1);
        testCar.setBrand("BMW");
        testCar.setCountry("GER");
        testCar.setCreated(new Timestamp(92835735));
        testCar.setUpdated(new Timestamp(94835735));
        testCar.setRegistration(new Timestamp(92835735));
        List<Car> carsMock=new ArrayList<>();
        carsMock.add(testCar);
        testCar.setId(2);
        testCar.setCreated(new Timestamp(95835735));
        testCar.setUpdated(new Timestamp(95835735));
        carsMock.add(testCar);

        Query query = mock(Query.class);
        when(entityManagerMock.createQuery("SELECT c FROM Car c ORDER BY c.updated asc")).thenReturn(query);
        // sort specified on query params
        String sort = "updated";
        // order specified on query params
        String order = "asc";
        when(query.getResultList()).thenReturn(carsMock);
        List carsOrderedService = carService.getCarsOrdered(sort, order);

        assertEquals(carsOrderedService,carsMock);
    }
    //Case 10: invalid query params
    @Test
    public void testGetCarsOrdered_invalid_query_params() {
        testCar = new Car();
        testCar.setId(1);
        testCar.setBrand("BMW");
        testCar.setCountry("GER");
        testCar.setCreated(new Timestamp(92835735));
        testCar.setUpdated(new Timestamp(94835735));
        testCar.setRegistration(new Timestamp(92835735));
        List<Car> carsMock=new ArrayList<>();
        carsMock.add(testCar);
        testCar.setId(2);
        testCar.setCreated(new Timestamp(95835735));
        testCar.setUpdated(new Timestamp(95835735));
        carsMock.add(testCar);

        // invalid sort specified on query params
        String sort = "invalid";
        // invalid order specified on query params
        String order = "notGood";
        assertThrows("please insert valid sort and order query params",IllegalArgumentException.class,()->carService.getCarsOrdered(sort, order));
    }
}
